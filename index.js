//console.log("hello ")

fetch('https://jsonplaceholder.typicode.com/todos')
 .then(response => response.json())
 .then((json) => {var titleElem = json.map((elem) => {
    return elem.title})
    console.log(titleElem)
});

////

async function fetchData(){

let result = await fetch('https://jsonplaceholder.typicode.com/todos/200')

let json = await result.json();
console.log(json)
console.log("The title is ipsam aperiam voluptates qui and the status is false");
};
fetchData();

////

fetch('https://jsonplaceholder.typicode.com/todos/',{
    method: "POST",
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: 'New todos using POST method',
        completed: false
    })
}).then(response => {
    return response.json()
})
.then(data => console.log(data));

////

fetch('https://jsonplaceholder.typicode.com/todos/1',{
    method: "PUT",
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: 'delectus aut autem',
        description: "Modified todos using PUT method",
        status: "done",
        completed: false,
        dateCompleted: "Nov 30, 2022",
        userId: 1
    })
}).then(response => {
    return response.json()
})
.then(data => console.log(data));

////

fetch('https://jsonplaceholder.typicode.com/todos/1',{
    method: "PATCH",
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        status: "complete",
        dateCompleted: "Dec 1, 2022",
    })
}).then(response => {
    return response.json()
})
.then(data => console.log(data));

////

fetch('https://jsonplaceholder.typicode.com/todos/2', {
    method: 'DELETE'
});